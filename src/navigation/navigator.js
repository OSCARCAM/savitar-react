import React from "react";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import { TouchableOpacity, Text } from "react-native";
import Ionicons from "@expo/vector-icons/Ionicons";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { useSelector, useDispatch } from "react-redux";
import { useForm } from "../hooks/useForm";
import { useContext, useEffect } from "react";
import { ApiContext } from "../context/ApiContext";

//importación de screens
import Ncotizacion from "../screens/nCotizacion";
import Historial from "../screens/historial";
import Login from "../screens/login";
import Npedido from "../screens/nPedido";

const Tab = createBottomTabNavigator();
const Stack = createNativeStackNavigator();



function MyTabs() {
  const { api } = useContext(ApiContext);
  const { logOut } = api;
  const userAuth = useSelector((state) => state.user);
  const dispatch = useDispatch();
  const auth = useSelector((state) => state.auth);

  const { user, password, onChange } = useForm({
    user: '',
    password: '',
  });


  return (
    <Tab.Navigator
      screenOptions={{
        "tabBarActiveTintColor": "#000",
        "tabBarInactiveTintColor": "#feb72b",
        "tabBarActiveBackgroundColor": "#feb72b",
        "tabBarInactiveBackgroundColor": "black",
        "tabBarStyle": [
          {
            "display": "flex"
          },
          null
        ]
      }}
    >
      <Tab.Screen
        name="NewP"
        component={Npedido}
        options={{
          title: "Nuevo Pedido",
          headerBackVisible: false,
          headerStyle: { backgroundColor: "black" },
          headerTintColor: "#F5B102",
          headerTitleStyle: { fontWeight: "bold" },
          headerRight: () => (
            <TouchableOpacity onPress={() => {dispatch(logOut());}} style={{ marginRight: 15 }}>
              <Ionicons name={"exit-outline"} size={30} color={"#F5B102"} />
            </TouchableOpacity>
          ),
          tabBarIcon: ({ focused, color, size }) => {
            return (
              <Ionicons
                name={focused ? "cash" : "cash-outline"}
                size={size}
                color={color}
              />
            );
          },
        }}
      />
      <Tab.Screen
        name="Cotizaciones"
        component={Ncotizacion}
        options={{
          title: "Nueva Cotización",
          headerBackVisible: false,
          headerStyle: { backgroundColor: "black" },
          headerTintColor: "#F5B102",
          headerTitleStyle: { fontWeight: "bold" },
          tabBarIcon: ({ focused, color, size }) => {
            return (
              <Ionicons
                name={focused ? "receipt" : "receipt-outline"}
                size={size}
                color={color}
              />
            );
          },
          headerRight: () => (
            <TouchableOpacity onPress={() => {dispatch(logOut())}} style={{ marginRight: 15 }}>
              <Ionicons name={"exit-outline"} size={30} color={"#F5B102"} />
            </TouchableOpacity>
          ),
        }}
      ></Tab.Screen>
      <Tab.Screen
        name="Historial"
        component={Historial}
        options={{
          title: "Historial",
          headerBackVisible: false,
          headerStyle: { backgroundColor: "black" },
          headerTintColor: "#F5B102",
          headerTitleStyle: { fontWeight: "bold" },
          tabBarIcon: ({ focused, color, size }) => {
            return (
              <Ionicons
                name={focused ? "time" : "time-outline"}
                size={size}
                color={color}
              />
            );
          },
          headerRight: () => (
            <TouchableOpacity onPress={() => {dispatch(logOut())}} style={{ marginRight: 15 }}>
              <Ionicons name={"exit-outline"} size={30} color={"#F5B102"} />
            </TouchableOpacity>
          ),
        }}
      ></Tab.Screen>
    </Tab.Navigator>
  );
}

export default function Navigator() {
  const auth = useSelector((state) => state.auth);

  return (
    <Stack.Navigator>
      {auth.auth != null ? 
        <Stack.Group screenOptions={{ headerShown: false }}>
          <Stack.Screen name="Home" component={MyTabs} />
        </Stack.Group>
       : 
        <Stack.Group screenOptions={{ headerShown: false }}>
          <Stack.Screen name="Login" component={Login} />
        </Stack.Group>
      }
    </Stack.Navigator>
  );
}

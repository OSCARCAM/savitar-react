import AsyncStorage from '@react-native-async-storage/async-storage';
import {savitarApi} from '../../api/savitarApi';
import store from './../Store';

export const signIn = (user, pwd) => async (dispatch) => {
    const data = {"USUARIO": user, "PASSWORD": pwd};
    try{

        //console.log({user, pwd});
        const res = await savitarApi.post(`/login`,data);
        

        if(res.data.msg){
            dispatch({type: 'user_not_registered', payload: 'Credenciales inválidas'});
            // console.log('Alerta: ' + res.data.msg)
        }else{
            let aux = res.data[0];
            aux.PASSWORD = pwd
            //console.log(res)
            dispatch({type: 'fetch_user_success', payload: aux});
            
            AsyncStorage.setItem('user', JSON.stringify(aux));
            //let user = await AsyncStorage.getItem('user')
            
        }
        
    }catch(error){
        dispatch({type: 'user_sign_in_failed', payload: 'Error al iniciar sesión'});
        console.log('err1 ' + error.msg);
    }
};

export const logOut = () => async (dispatch) => {
    await AsyncStorage.removeItem('user');
    dispatch({type: 'user_sign_out', payload: null});
}; 


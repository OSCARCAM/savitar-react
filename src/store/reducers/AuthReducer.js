export const initialAuthState = {
    auth: null,
    loading: false,
    error: {
        flag: false,
        msg: null
    }
}

export const AuthReducer = (state = initialAuthState, action) => {
    switch(action.type){
        case 'fetch_user':
            return {
                ...state,
                loading: true
            };
        case 'fetch_user_success':
            return {
                ...state,
                auth: action.payload,
                loading: false,
                error: {
                    flag: false,
                    msg: null
                }
            };
        case 'user_not_registered':
            return {
                ...state,
                loading: false,
                error: {
                    flag: true,
                    msg: action.payload
                },
                auth: null
            };
        case 'user_sign_in':
            return {
                ...state,
                loading: true
            };
        case 'user_sign_in_failed':
            return {
                ...state,
                auth: null,
                loading: false,
                error: {
                    flag: true,
                    msg: action.payload
                }
            };
        case 'user_sign_out':
            return initialAuthState;
        default:
            return state;
    }
}
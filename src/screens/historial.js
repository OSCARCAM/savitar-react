import {
  View,
  Text,
  StyleSheet,
  Dimensions,
  TextInput,
  TouchableOpacity,
  FlatList,
  Share,
  Linking,
  ActivityIndicator
} from "react-native";
import SelectDropdown from "react-native-select-dropdown";
import Ionicons from "@expo/vector-icons/Ionicons";
import { useSelector, useDispatch } from "react-redux";
import { useForm } from "../hooks/useForm";
import React, { useContext, useEffect, useState } from "react";
import { ApiContext } from "../context/ApiContext";
import { savitarApi } from "../api/savitarApi";
import AsyncStorage from "@react-native-async-storage/async-storage";

export default function Historial() {
  const [tipos, setTipos] = useState();
  const [tipo, setTipo] = useState(null);
  const [id_venta, setId_venta] = useState(null);
  const [ventas, setVentas] = useState([]);
  const [clientes, setClientes] = useState([]);
  const [infoclientes, setInfoClientes] = useState([]);
  const [cliente, setCliente] = useState(null);
  const [refreshing, setRefreshing] = useState(false);
  const [loading, setLoading] = useState(true);

  var cantidad = 50;
  var inicio = 1;
  var fin = cantidad;

  const auth = useSelector((state) => state.auth);

  useEffect(() => {
    setTimeout(() => {
      setLoading(false);
    }, 350);
    console.log(
      "-----------------------------------------------------------------------------------------------------------"
    ),
      initVentasTipo();
  }, []);

  const loadData = () => {
    inicio = inicio + cantidad;
    fin = fin + inicio;

    initVentas(e);
  };

  const buscador = () => {
    inicio = 1;
    fin = cantidad;
    initVentas();
    setRefreshing(true);
  };

  const initVentasTipo = async () => {
    let tv = await savitarApi.post(`obtenerTiposVentas`, "");
    let datos = tv.data;
    let filter = datos.map((x) => {
      return x.TIPO_VENTA;
    });

    setTipos(filter);
    initVentas();
  };

  const initVentas = async (e = null) => {
    setRefreshing(true);

    let buscar = {
      BUSCADOR: "WHERE V.VENDEDOR = " + auth.auth.NOMINA,
      INICIO: inicio,
      FIN: fin,
    };

    if (tipo != null) {
      buscar.BUSCADOR = buscar.BUSCADOR + " AND V.ID_TIPO_VENTA = " + tipo;
    }

    if (id_venta != null) {
      buscar.BUSCADOR = buscar.BUSCADOR + " AND V.ID_VENTA = " + id_venta;
    }

    if (cliente != null) {
      buscar.BUSCADOR = buscar.BUSCADOR + " AND C.ID_CLIENTE = " + cliente;
    }

    let resventas = await savitarApi.post(`obtenerVentas`, buscar);
    let arrventas = resventas.data;

    /*
    arrventas.map((x) => {
      ventas.push(x);
    });   */
    setVentas(arrventas);
    setRefreshing(true);
    initClientes();
  };

  const initClientes = async () => {
    let data = {
      ID_VENDEDOR: auth.auth.NOMINA,
    };

    let clients = await savitarApi.post(`getClients`, data);
    let info = clients.data;
    let nomclient = info.map((x) => {
      return x.NOMBRE;
    });

    if (clients) {
      setClientes(nomclient);
      setInfoClientes(info);
    }
  };
  const load = () => renderItem();

  const onShare = async () => {
    try {
      const result = await Share.share({
        message: "https://savitar.app/Docs/pedidos/" + id + ".pdf",
      });
      if (result.action === Share.sharedAction) {
        if (result.activityType) {
          console.log("qué quieres hacer?");
        } else {
          console.log("si se mandó");
        }
      } else if (result.action === Share.dismissedAction) {
        console.log("se canceló");
      }
    } catch (error) {
      alert(error.message);
    }
  };

  const renderItem = ({ item, index }) => {
    setRefreshing(false);
    return (
      <View style={styles.card}>
        <View style={{ flexDirection: "row", marginBottom: 10 }}>
          <Text style={{ fontWeight: "bold" }}>No Venta:</Text>
          <Text>{item.ID_VENTA}</Text>
          <Text style={{ marginLeft: "50%" }}>{item.TIPO_VENTA}</Text>
        </View>
        <View style={{ flexDirection: "row", marginBottom: 10 }}>
          <Text style={{ fontWeight: "bold" }}>Vendedor:</Text>
          <Text>{item.NOMBRE_VENDEDOR}</Text>
        </View>
        <View style={{ flexDirection: "row", marginBottom: 10, maxWidth: 270 }}>
          <Text style={{ fontWeight: "bold" }}>Cliente:</Text>
          <Text>{item.NOMBRE}</Text>
        </View>
        <View style={{ flexDirection: "row", marginBottom: 10 }}>
          <Text style={{ fontWeight: "bold" }}>Status:</Text>
          <Text>{item.ESTATUS}</Text>
        </View>
        <View style={{ flexDirection: "row" }}>
          <Text style={{ fontWeight: "bold" }}>Subtotal:</Text>
          <Text>{item.SUBTOTAL}</Text>
          <Text style={{ fontWeight: "bold", marginLeft: "50%" }}>Total:</Text>
          <Text>{item.IMPORTE}</Text>
        </View>
        <View style={{ flexDirection: "row", marginTop: 10 }}>
          <TouchableOpacity
            style={{
              justifyContent: "center",
              alignItems: "center",
              marginRight: 10,
              backgroundColor: "#F5B102",
              padding: 3,
              width: "15%",
              height: 40,
              borderRadius: 20,
              alignSelf: "center",
              overflow: "visible",
            }}
            onPress={async () => {
              try {
                const result = await Share.share({
                  message:
                    "Este es el enlace de descarga del ticket con el numero de venta" +
                    item.ID_VENTA +
                    "https://savitar.app/Docs/pedidos/" +
                    item.ID_VENTA +
                    ".pdf",
                });
                if (result.action === Share.sharedAction) {
                  if (result.activityType) {
                    console.log("qué quieres hacer?");
                  } else {
                    console.log("si se mandó");
                  }
                } else if (result.action === Share.dismissedAction) {
                  console.log("se canceló");
                }
              } catch (error) {
                alert(error.message);
              }
            }}
          >
            <Ionicons name={"share-social-outline"} color={"black"} size={25} />
          </TouchableOpacity>
          <TouchableOpacity
            style={{
              justifyContent: "center",
              alignItems: "center",

              backgroundColor: "black",
              padding: 3,
              width: "15%",
              height: 40,
              borderRadius: 20,
              alignSelf: "center",
              overflow: "visible",
              elevation: 8,
            }}
            onPress={() =>
              Linking.openURL(`mailto:""?subject=Ticket&body=Este es el enlace de descarga del ticket con el numero de venta ${item.ID_VENTA} 
            https://savitar.app/Docs/pedidos/${item.ID_VENTA}.pdf`)
            }
          >
            <Ionicons name={"mail-open-outline"} color={"#F5B102"} size={25} />
          </TouchableOpacity>
        </View>
      </View>
    );
  };

  return (
    <View style={styles.container}>
      {loading ? (
        <ActivityIndicator color="#F5B102"></ActivityIndicator>
      ) : (
        <>
          <View style={{ flexDirection: "row", marginTop: 15 }}>
            <SelectDropdown
              buttonStyle={styles.tipo}
              data={tipos}
              onSelect={(selectedItem, index) => {
                if (selectedItem == "Venta") {
                  setTipo(1);
                } else {
                  setTipo(2);
                }
              }}
              defaultButtonText="Tipo de Venta"
              buttonTextAfterSelection={(selectedItem, index) => {
                // text represented after item is selected
                // if data array is an array of objects then return selectedItem.property to render after item is selected
                return selectedItem;
              }}
              rowTextForSelection={(item, index) => {
                // text represented for each item in dropdown
                // if data array is an array of objects then return item.property to represent item in dropdown
                return item;
              }}
              buttonTextStyle={styles.dropdown1BtnTxtStyle}
              renderDropdownIcon={(isOpened) => {
                return (
                  <Ionicons
                    name={isOpened ? "chevron-up" : "chevron-down"}
                    color={"#444"}
                    size={18}
                  />
                );
              }}
              dropdownIconPosition={"right"}
              dropdownStyle={styles.dropdown1DropdownStyle}
              rowStyle={styles.dropdown1RowStyle}
              rowTextStyle={styles.dropdown1RowTxtStyle}
              selectedRowStyle={styles.dropdown1SelectedRowStyle}
            />
            <TextInput
              placeholder="N° de Venta"
              style={styles.txtNo}
              onChange={setId_venta}
              value={id_venta}
            ></TextInput>
          </View>
          <Text style={{ fontSize: 15, marginTop: 15, marginLeft: 10 }}>
            Cliente
          </Text>
          <SelectDropdown
            buttonStyle={{
              width: "95%",
              alignSelf: "center",
              backgroundColor: "white",
              borderColor: "#F0F0F0",
              borderWidth: 1,
            }}
            data={clientes}
            onSelect={(selectedItem, index) => {
              let filtro = infoclientes.filter((x) => x.NOMBRE == selectedItem);
              let cliID = filtro[0].ID_CLIENTE;

              setCliente(cliID);
            }}
            defaultButtonText={"Seleccione el cliente"}
            buttonTextAfterSelection={(selectedItem, index) => {
              return selectedItem;
            }}
            rowTextForSelection={(item, index) => {
              return item;
            }}
            buttonTextStyle={styles.dropdown1BtnTxtStyle}
            renderDropdownIcon={(isOpened) => {
              return (
                <Ionicons
                  name={isOpened ? "chevron-up" : "chevron-down"}
                  color={"#444"}
                  size={18}
                />
              );
            }}
            dropdownIconPosition={"right"}
            dropdownStyle={styles.dropdown1DropdownStyle}
            rowStyle={styles.dropdown1RowStyle}
            rowTextStyle={styles.dropdown1RowTxtStyle}
            selectedRowStyle={styles.dropdown1SelectedRowStyle}
            search
            searchInputStyle={styles.dropdown1searchInputStyleStyle}
            searchPlaceHolder={"Search here"}
            searchPlaceHolderColor={"darkgrey"}
            renderSearchInputLeftIcon={() => {
              return <Ionicons name={"search"} color={"#444"} size={18} />;
            }}
          />
          <TouchableOpacity style={styles.btnFinal} onPress={buscador}>
            <Text style={{ color: "white" }}>Buscar</Text>
          </TouchableOpacity>
          <View
            style={
              ventas.length == 0
                ? { hide: "true" }
                : {
                    backgroundColor: "white",
                    marginTop: 20,
                    height: "60%",
                    justifyContent: "center",
                    alignItems: "center",
                  }
            }
          >
            <FlatList
              data={ventas}
              keyExtractor={(item) => item.ID_VENTA}
              renderItem={renderItem}
              refreshing={refreshing}
              onRefresh={load}
              style={{
                width: "98%",
                marginBottom: 35,
                alignSelf: "center",
                backgroundColor: "white",
              }}
            ></FlatList>
          </View>
          <View
            style={
              ventas.length != 0 ? { hide: "true" } : { alignItems: "center" }
            }
          >
            <Text style={{ color: "black" }}>No se encontró info</Text>
          </View>
        </>
      )}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    height: Dimensions.get("window").height - 1,
    backgroundColor: "#FFFFFF",
  },
  tipo: {
    backgroundColor: "#FFFFFF",
    marginLeft: 15,
    marginRight: 10,
    width: "45%",
    color: "black",
    borderWidth: 1,
    borderColor: "#F0F0F0",
  },
  btnFinal: {
    justifyContent: "center",
    alignItems: "center",
    marginTop: 20,
    backgroundColor: "#F5B102",
    padding: 7,
    width: "90%",
    height: 40,
    borderRadius: 20,
    alignSelf: "center",
    overflow: "visible",
    elevation: 8, // Android
  },
  txtNo: {
    color: "black",
    borderWidth: 1,
    borderColor: "#F0F0F0",
    fontWeight: "bold",
    fontSize: 14,
    width: "45%",
    padding: 7,
  },
  card: {
    borderWidth: 1.5,
    borderColor: "#F0F0F0",
    margin: 10,
    borderRadius: 10,
    padding: 10,
  },
  dropdown1BtnTxtStyle: { color: "#444", textAlign: "left", fontSize: 15 },
  dropdown1DropdownStyle: { backgroundColor: "#fff" },
  dropdown1RowStyle: {
    backgroundColor: "#fff",
    borderBottomColor: "#C5C5C5",
  },
  dropdown1RowTxtStyle: { color: "#444", textAlign: "left", fontSize: 15 },
  dropdown1SelectedRowStyle: { backgroundColor: "rgba(0,0,0,0.1)" },
  dropdown1searchInputStyleStyle: {
    backgroundColor: "#fff",
    borderRadius: 8,
    borderBottomWidth: 1,
    borderBottomColor: "#444",
  },
});

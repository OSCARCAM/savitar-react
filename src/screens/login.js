import {
  View,
  Text,
  TextInput,
  StyleSheet,
  Image,
  TouchableOpacity,
  ActivityIndicator,
} from "react-native";
import { StatusBar } from "expo-status-bar";
import logo from "../../assets/SAVITAR.png";
import { useSelector, useDispatch } from "react-redux";
import { useForm } from "../hooks/useForm";
import { useContext, useEffect,useState } from "react";
import { ApiContext } from "../context/ApiContext";
import Toast from "react-native-root-toast";


export default function Login() {
  const { api } = useContext(ApiContext);
  const { signIn } = api;
  const userAuth = useSelector((state) => state.user);
  const dispatch = useDispatch();
  const auth = useSelector((state) => state.auth);
  const [load, setLoad] = useState(false);


  const { user, password, onChange } = useForm({
    user: '',
    password: '',
  });

  useEffect(() => {
    if (auth.error && auth.error.flag && auth.error.msg) {
      Toast.show(auth.error.msg, {
        duration: Toast.durations.SHORT,
        position: Toast.positions.BOTTOM,
        shadow: true,
        animation: true,
        hideOnPress: true,
        delay: 0,
        containerStyle: { borderRadius: 20 },
      });
    }
  }, [auth.error]);

  const onLogin = async () => {
    //console.log({bla:'hola!',user, password});

    if (user == "" || password == "") {
     // console.log(user, password);
      Toast.show("Campos inválidos", {
        duration: Toast.durations.SHORT,
        position: Toast.positions.BOTTOM,
        shadow: true,
        animation: true,
        hideOnPress: true,
        delay: 0,
        containerStyle: { borderRadius: 20 },
      });
    } else {
      //console.log(user, password);
      //Keyboard.dismiss();
      setTimeout(() => {
        setLoad(true)
    }, 350);
      dispatch(signIn(user, password));
    }

    /*
    if(state.user && state.password){
        dispatch(consoel.log(state.user, state.password));
    }else{
        Alert.alert('Error', 'Verifique que los campos están rellenos correctamente');
    }
    */
  };

  return (
    
    <View style={styles.container}>
      <Image style={styles.tinyLogo} source={logo} />
      <Text style={styles.labelCampos}>Correo</Text>
      <TextInput
        style={styles.campos}
        value={user}
        onChangeText={(value) => onChange(value, 'user')}
      ></TextInput>
      <Text style={styles.labelCampos}>Contraseña</Text>
      <TextInput
        value={password}
        onChangeText={(value) => onChange(value, 'password')}
        secureTextEntry={true}
        style={styles.campos}
      ></TextInput>
      
      <TouchableOpacity onPress={() => {onLogin()}} style={styles.btningrsar}>
        <Text style={styles.buttonText}>INGRESAR </Text>
      </TouchableOpacity>
      <ActivityIndicator animating={load} color='#F5B102' />
      <StatusBar style="dark" />
    </View>
  );
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    justifyContent: "center",
  },
  btningrsar: {
    backgroundColor: "#FFC300",
    padding: 7,
    borderRadius: 35,
    width: 150,
    alignItems: "center",
    alignSelf: "center",
    marginTop: 25,
  },
  buttonText: {
    color: "#fff",
  },
  labelCampos: {
    marginLeft: 15,
    marginTop: 15,
    color: "#FFC300",
  },
  campos: {
    width: "95%",
    height: 35,
    backgroundColor: "white",
    borderWidth: 1,
    borderColor: "#F0F0F0",
    borderRadius: 30,
    marginTop: 10,
    marginLeft: 10,
    padding: 10,
    color: "#FFC300",
  },
  tinyLogo: {
    marginTop: -100,
    width: 350,
    height: 350,
  },
});

import {
  View,
  Text,
  TextInput,
  StyleSheet,
  ScrollView,
  TouchableOpacity,
  Alert,
} from "react-native";
import { CheckBox } from "@rneui/themed";
import Ionicons from "@expo/vector-icons/Ionicons";
import { useForm } from "../hooks/useForm";
import { useContext, useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { ApiContext } from "../context/ApiContext";
import { savitarApi } from "../api/savitarApi";
import SelectDropdown from "react-native-select-dropdown";
import React from "react";

export default function Clientes({ closeModal }) {
  const [reqRFC, setReqRFC] = useState(false);
  const [nombre, setNombre] = useState('');
  const [telefono, setTelefono] = useState('');
  const [RFC, setRFC] = useState('');
  const [razon, setRazon] = useState('');
  const [correo, setCorreo] = useState('');
  const [dias, setDias] = useState('');
  const [calle, setCalle] = useState('');
  const [nExt, setNExt] = useState('');
  const [nInt, setNInt] = useState('');
  const [colonia, setColonia] = useState('');
  const [cp, setCP] = useState('');
  const [estado, setEstado] = useState('');
  const [municipio, setMunicipio] = useState('');
  const [puntoVenta, setPuntoVenta] = useState();
  const [usuario, setUsuario] = useState();
  const [municipios, setMunicipios] = useState([]);
  const [munEdo, setMunEdo] = useState([]);
  const [munInfo, setMunInfo] = useState([]);
  const [estados, setEstados] = useState([]);
  const [estInfo, setEstInfo] = useState([]);
  const [id_estado, setId_estado] = useState('');
  const [id_municipio, setId_municipio] = useState('');

  const { api } = useContext(ApiContext);
  const auth = useSelector((state) => state.auth);

  useEffect(() => {
    console.log(
      "-------------------------------------------------------------------------------------"
    );
    initEstados();
    initMunicipio();
  }, []);

  const initEstados = async () => {
    try {
      let est = await savitarApi.post(`estados`, "");

      est.data.forEach((x) => {
        if (x.ID_ESTADO > 0) {
          estInfo.push(x);
          estados.push(x.DESCRIPCION_ESTADO);
        }
      });
    } catch (error) {
      console.log(error);
    }
  };

  const initMunicipio = async () => {
    try {
      let mun = await savitarApi.post(`municipios`, "");
      setMunInfo(mun.data);
      setMunicipios([]);
      let aux = mun.data.map((x) => {
        return x.DESCRIPCION_MUNICIPIO;
      });
      setMunicipios(aux);
    } catch (error) {
      console.log(error);
    }
  };

  const cambiaEstado = async (id) => {
    let mun = [];

    mun = munInfo.filter((x) => x.ID_ESTADO == id);
    let aux = mun.map((x) => x.DESCRIPCION_MUNICIPIO);
    setMunEdo(aux);
    //console.log(mun)
  };

  const createClient = async () => {
    let data = {
      NOMBRE: nombre,
      TELEFONO: telefono,
      RFC: RFC,
      EMAIL: correo,
      DIAS_CREDITO: dias,
      REQUIERE_FACTURA: reqRFC,
      PUNTO_VENTA: auth.auth.ID_PUNTO_VENTA,
      USUARIO: auth.auth.NOMINA,

      CALLE: calle,
      NUM_EXT: nExt,
      NUM_INT: nInt,
      COLONIA: colonia,
      CODIGO: cp,
      ID_MUNICIPIO: id_municipio,
      ID_ESTADO: id_estado,
    };

    let res = await savitarApi.post("insertClient", data).then(createAlert());
  };
  const createAlert = () => {
    Alert.alert("", "Se ha generado el cliente", [
      {
        text: "Aceptar",
        onPress: () => {
          setNombre('');
          setTelefono('');
          setRFC('');
          setCorreo('');
          setDias('');
          setReqRFC(false);
          setCalle('');
          setNExt('');
          setNInt('');
          setColonia('');
          setCP('');
          setId_estado('');
          setId_municipio('');
          closeModal();
        },
      },
    ]);
  };

  return (
    <View style={styles.modalContent}>
      <ScrollView>
        <View
          style={{
            backgroundColor: "white",
            paddingLeft: 10,
            paddingRight: 10,
          }}
        >
          <Text style={styles.labelModal}>Nombre</Text>
          <TextInput
            placeholder="Nombre"
            onChangeText={(x) => {
              setNombre(x);
            }}
            value={{ nombre }}
            style={styles.inputModal}
          />
          <Text style={styles.labelModal}>Telefono</Text>
          <TextInput
            placeholder="Telefono"
            onChangeText={setTelefono}
            value={{ telefono }}
            style={styles.inputModal}
            keyboardType={"number-pad"}
          />
          <CheckBox
            title="Requiere Factura..."
            checked={reqRFC}
            containerStyle={{
              backgroundColor: "white",
              padding: 5,
              marginLeft: 0,
            }}
            checkedColor={"#F5B102"}
            onPress={() => (reqRFC ? setReqRFC(false) : setReqRFC(true))}
          />
          {reqRFC ? (
            <View
              style={{
                margin: 5,
                borderTopColor: "#f0f0f0",
                borderTopWidth: 2,
              }}
            >
              <Text style={styles.labelModal}>RFC</Text>
              <TextInput
                placeholder="RFC"
                onChangeText={setRFC}
                value={RFC}
                style={styles.inputModal}
              />
              <Text style={styles.labelModal}>Razón Social</Text>
              <TextInput
                placeholder="Razón Social"
                onChangeText={setRazon}
                value={razon}
                style={styles.inputModal}
              />
              <Text style={styles.labelModal}>Correo</Text>
              <TextInput
                placeholder="Correo"
                onChangeText={setCorreo}
                value={correo}
                keyboardType={'email-address'}
                style={styles.inputModal}
              />
              <Text style={styles.labelModal}>Días de Credito</Text>
              <TextInput
                placeholder="Dias de Credito"
                onChangeText={setDias}
                value={dias}
                style={styles.inputModal}
                keyboardType={"number-pad"}
              />

              <Text>Dirección</Text>
              <Text style={styles.labelModal}>Calle</Text>
              <TextInput
                placeholder="Calle"
                onChangeText={setCalle}
                value={calle}
                style={styles.inputModal}
              />
              <Text style={styles.labelModal}>Número Exterior</Text>
              <TextInput
                placeholder="Número Exterior"
                onChangeText={setNExt}
                value={nExt}
                style={styles.inputModal}
                keyboardType={"number-pad"}
              />
              <Text style={styles.labelModal}>Número Interior</Text>
              <TextInput
                placeholder="Número Interior"
                value={{nInt}}
                style={styles.inputModal}
                keyboardType={"number-pad"}
              />
              <Text style={styles.labelModal}>Colonia</Text>
              <TextInput
                placeholder="Colonia"
                onChangeText={setColonia}
                value={colonia}
                style={styles.inputModal}
              />
              <Text style={styles.labelModal}>Código Postal</Text>
              <TextInput
                placeholder="Código Postal"
                onChangeText={setCP}
                value={cp}
                style={styles.inputModal}
                keyboardType={"number-pad"}
              />
              <Text style={styles.labelModal}>Estado</Text>

              <SelectDropdown
                data={estados}
                //disableAutoScroll="true"
                //onScrollEndReached={()=>loadMore()}
                onSelect={async (selectedItem, index) => {
                  let filtro = estInfo.find(
                    (x) => x.DESCRIPCION_ESTADO == selectedItem
                  );
                  let id_est = filtro.ID_ESTADO;
                  console.log(id_est);
                  setId_estado(id_est);
                  cambiaEstado(id_est);
                }}
                defaultButtonText={"Seleccione el estado"}
                buttonTextAfterSelection={(selectedItem, index) => {
                  return selectedItem;
                }}
                rowTextForSelection={(item, index) => {
                  return item;
                }}
                buttonStyle={styles.btnCliente}
                buttonTextStyle={styles.dropdown1BtnTxtStyle}
                renderDropdownIcon={(isOpened) => {
                  return (
                    <Ionicons
                      name={isOpened ? "chevron-up" : "chevron-down"}
                      color={"#444"}
                      size={18}
                    />
                  );
                }}
                dropdownIconPosition={"right"}
                dropdownStyle={styles.dropdown1DropdownStyle}
                rowStyle={styles.dropdown1RowStyle}
                rowTextStyle={styles.dropdown1RowTxtStyle}
                selectedRowStyle={styles.dropdown1SelectedRowStyle}
                search
                searchInputStyle={styles.dropdown1searchInputStyleStyle}
                searchPlaceHolder={"Search here"}
                searchPlaceHolderColor={"darkgrey"}
                renderSearchInputLeftIcon={() => {
                  return <Ionicons name={"search"} color={"#444"} size={18} />;
                }}
              />
              <Text style={styles.labelModal}>Municipio</Text>
              <SelectDropdown
                data={munEdo}
                //disableAutoScroll="true"
                //onScrollEndReached={()=>loadMore()}
                onSelect={async (selectedItem, index) => {
                  let filtro = munInfo.find(
                    (x) => x.DESCRIPCION_MUNICIPIO == selectedItem
                  );
                  let id_mun = filtro.ID_MUNICIPIO;
                  console.log(id_mun);
                  setId_municipio(id_mun);
                }}
                defaultButtonText={"Seleccione el municipio"}
                buttonTextAfterSelection={(selectedItem, index) => {
                  return selectedItem;
                }}
                rowTextForSelection={(item, index) => {
                  return item;
                }}
                buttonStyle={styles.btnCliente}
                buttonTextStyle={styles.dropdown1BtnTxtStyle}
                renderDropdownIcon={(isOpened) => {
                  return (
                    <Ionicons
                      name={isOpened ? "chevron-up" : "chevron-down"}
                      color={"#444"}
                      size={18}
                    />
                  );
                }}
                dropdownIconPosition={"right"}
                dropdownStyle={styles.dropdown1DropdownStyle}
                rowStyle={styles.dropdown1RowStyle}
                rowTextStyle={styles.dropdown1RowTxtStyle}
                selectedRowStyle={styles.dropdown1SelectedRowStyle}
                search
                searchInputStyle={styles.dropdown1searchInputStyleStyle}
                searchPlaceHolder={"Search here"}
                searchPlaceHolderColor={"darkgrey"}
                renderSearchInputLeftIcon={() => {
                  return <Ionicons name={"search"} color={"#444"} size={18} />;
                }}
              />
            </View>
          ) : ''}

          <TouchableOpacity
            style={styles.guardar}
            onPress={() => {
              console.log(reqRFC,nombre,telefono,correo,RFC, calle, colonia, cp, razon, dias,nExt,nInt,id_estado,id_municipio)
              if (!reqRFC && nombre != '' && telefono != '') {
                //console.log('si pasa')
                createClient();
              } else if (
                reqRFC &&
                nombre != '' &&
                telefono != '' &&
                correo != '' &&
                RFC != '' &&
                calle != '' &&
                colonia != '' &&
                cp != '' &&
                razon != '' &&
                dias != '' &&
                nExt != '' &&
                nInt != '' &&
                id_estado != '' &&
                id_municipio != ''
              ) {
                //console.log('si pasa v2')
                createClient();
              } else {
                Alert.alert('', "No ha completado uno de los campos", [
                  {
                    text: "Aceptar",
                    onPress: () => {},
                  },
                ]);
              }
            }}
          >
            <Text style={{ color: "white" }}>Guardar Cliente</Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  modalContent: {
    flex: 1,
  },
  modalToggle: {
    justifyContent: "center",
    alignItems: "center",
    marginBottom: 10,
    borderWidth: 1,
    borderColor: "#f2f2f2",
    padding: 10,
    borderRadius: 10,
    alignSelf: "center",
  },
  modalClose: {
    marginTop: 20,
    marginBottom: 0,
  },
  labelModal: {
    color: "#F5B102",
    fontSize: 12,
    marginTop: 10,
  },
  inputModal: {
    borderWidth: 1,
    borderColor: "#f0f0f0",
    borderRadius: 10,
    width: 300,
    height: 30,
    padding: 7,
    marginTop: 5,
  },
  guardar: {
    width: "90%",
    backgroundColor: "#F5B102",
    height: 30,
    alignSelf: "center",
    alignItems: "center",
    justifyContent: "center",
    marginTop: 20,
    marginBottom: 20,
    borderRadius: 15,
  },
  dropdown1BtnTxtStyle: { color: "#444", textAlign: "left", fontSize: 12 },
  dropdown1DropdownStyle: { backgroundColor: "#EFEFEF", height: 300 },
  dropdown1RowStyle: {
    backgroundColor: "#EFEFEF",
    borderBottomColor: "#C5C5C5",
  },
  dropdown1RowTxtStyle: { color: "#444", textAlign: "left", fontSize: 12 },
  dropdown1SelectedRowStyle: { backgroundColor: "rgba(0,0,0,0.1)" },
  dropdown1searchInputStyleStyle: {
    backgroundColor: "#EFEFEF",
    borderRadius: 8,
    borderBottomWidth: 1,
    borderBottomColor: "#444",
  },
  btnCliente: {
    borderWidth: 1,
    borderColor: "#E3E3E3",
    borderRadius: 10,
    margin: 10,
    width: "94%",
    height: 35,
    backgroundColor: "white",
  },
  btnDisable: {
    width: "90%",
    backgroundColor: "#F5B102",
    height: 30,
    alignSelf: "center",
    alignItems: "center",
    justifyContent: "center",
    marginTop: 20,
    marginBottom: 20,
    borderRadius: 15,
    opacity: 0.5,
  },
});

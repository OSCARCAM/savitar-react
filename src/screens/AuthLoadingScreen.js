import { ActivityIndicator, Dimensions, ImageBackground, View } from "react-native";

export default function AuthLoadingScreen(props){
    return (
        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
            <ImageBackground 
                style={{position: 'absolute', left: 0, top: 0, width: Dimensions.get('screen').width, height: Dimensions.get('screen').height, justifyContent: 'flex-end', alignItems: 'center'}}
                resizeMode="cover"
                source={require('../../assets/splash.png/')}
            >
                <ActivityIndicator style={{ paddingBottom: 100 }} color={'white'} size="large"/>
            </ImageBackground>
        </View>
    );
}
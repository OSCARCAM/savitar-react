import React from "react";
import { StatusBar } from "expo-status-bar";
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  Dimensions,
  TextInput,
  FlatList,
  Modal,
  Alert,
  TouchableWithoutFeedback,
  ActivityIndicator,
  TouchableHighlight,
} from "react-native";
import axios from "axios";
import { Button, Header, CheckBox } from "@rneui/themed";
import Ionicons from "@expo/vector-icons/Ionicons";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { useSelector, useDispatch } from "react-redux";

import { useContext, useEffect, useState } from "react";
import { ApiContext } from "../context/ApiContext";
import { savitarApi, savitarTicket } from "../api/savitarApi";
import SelectDropdown from "react-native-select-dropdown";

import Clientes from "./clientes";

export default function Npedido({ navigation }) {
  const [clientes, setClientes] = useState([]);
  const [infoClientes, setInfoClientes] = useState([]);
  const [clientID, setClientID] = useState([]); //array de clientes con su ID
  const [producto, setProducto] = useState([]);
  const [id_cliente, setID_Cliente] = useState("");
  const [refreshing, setRefreshing] = useState(false);
  const [buscador, setBuscador] = useState();
  const [productos, setProductos] = useState([]);
  const [total, setTotal] = useState();
  const [modalVisible, setModalVisible] = useState(false);
  const [modal2Visible, setModal2Visible] = useState(false);
  const [isChecked, setIsChecked] = useState(false);
  const [actualIndex, setActualIndex] = useState(0);
  const [nomProd, setNomProd] = useState([]);

  //Constantes de el listado de productos
  const [productosList, setProductosList] = useState([]);
  const [prodInfo, setProdInfo] = useState([]);
  const [pu, setPU] = useState([{ precio: 0, index: 0 }]);
  const [cant, setCant] = useState([{ cantidad: 1, index: 0 }]);
  const [nom, setNom] = useState([{ nombre: "", index: 0 }]);
  const [subtotal, setSubtotal] = useState(0);
  const [inicio, setInicio] = useState(1);
  const [fin, setFin] = useState(10);
  const [loading, setLoading] = useState(false);

  const startLoading = () => {
    setLoading(true);
    setTimeout(() => {
      setLoading(false);
    }, 3000);
  };
  let leftOpenValue = Dimensions.get("window").width;

  const cantidadProductos = 10;

  const { api } = useContext(ApiContext);
  const { signIn } = api;
  const userAuth = useSelector((state) => state.user);
  const dispatch = useDispatch();
  const auth = useSelector((state) => state.auth);

  useEffect(() => {
    initClientes();
    initProductos(inicio, fin);
    setID_Cliente(null);
    console.log(
      "-------------------------------------------------------------"
    );
  }, []);

  useEffect(() => {
    calcSubtotal();
  }, [subtotal]);

  useEffect(() => {
    calcSubtotal();
  }, [cant]);

  useEffect(() => {
    if (inicio == 1) {
    } else {
      initProductos();
    }
  }, [inicio]);

  const ListFooterComponent = () => {
    return (
      <View>
        <TouchableWithoutFeedback>
          <ActivityIndicator color="#F5B102"></ActivityIndicator>
        </TouchableWithoutFeedback>
      </View>
    );
  };
  const initClientes = async () => {
    //AsyncStorage.setItem('subtotal', '0')
    let totals = AsyncStorage.getItem("subtotal");
    let info = {
      ID_VENDEDOR: auth.auth.NOMINA,
    };

    try {
      const res = await savitarApi.post(`getClients`, { info });

      if (res.data) {
        let datos = res.data;
        setInfoClientes(datos);

        let clientes1 = datos.map((x) => {
          return x.NOMBRE;
        });
        setClientes(clientes1);

        let clientes2 = datos.map((cli) => {
          let json = { Nombre: cli.NOMBRE, Id: cli.ID_CLIENTE };
          return json;
        });
        setClientID(clientes2);
      }
    } catch (error) {
      console.log("err" + error);
    }
  };

  const initProductos = async (e = null) => {
    let localID = await AsyncStorage.getItem("id_cliente");
    setID_Cliente(localID);

    let info = {
      ID_PUNTO_VENTA: auth.auth.ID_PUNTO_VENTA,
      INICIO: inicio,
      FIN: inicio + cantidadProductos,
      BUSCADOR: e,
      ID_CLIENTE: id_cliente ? id_cliente : null,
    };

    try {
      const res = await savitarApi.post(`/obtenerProductos`, info);
      if (buscador == null) {
        if (res.data.length > 0) {
          let prod = res.data;
          setProdInfo([...prodInfo, ...prod]); //Aquí se almacena toda la informacion del producto
          let aux = prod.map((x) => {
            return x.DESCRIPCION_MODELO;
          });
          setProductos([...productos, ...aux]);
        } else {
          console.log("no llega nada");
        }
      } else {
        if (res.data.length > 0) {
          let prod = res.data;
          setProdInfo(prod); //Aquí se almacena toda la informacion del producto
          let aux = prod.map((x) => {
            return x.DESCRIPCION_MODELO;
          });
          setProductos(aux);
        }
      }
    } catch (error) {
      console.log("err" + error);
    }
    setFin(inicio + cantidadProductos);
  };

  const calcSubtotal = async () => {
    let precios = pu.map((x) => x.precio);
    let cantidades = cant.map((x) => x.cantidad);
    let suma = 0;

    for (let i = 0; i < precios.length && i < cantidades.length; i++) {
      suma += cantidades[i] * precios[i];
    }
    setSubtotal(suma);
    //console.log(subtotal)
  };

  const agregarProducto = async () => {
    let data = {};

    if (producto.length <= 0) {
      data.id = 1;
      data.ID_VENTA = null;
      data.DESCRIPCION = null;
      data.NO_PARTE = null;
      data.ID_PUNTO_VENTA = null;
      data.CANTIDAD = 1;
      data.PRECIO = 0;
      data.ID_USUARIO = auth.auth.NOMINA;
    } else {
      data.id = producto[producto.length - 1].id + 1;
      data.ID_VENTA = null;
      data.DESCRIPCION = null;
      data.NO_PARTE = null;
      data.ID_PUNTO_VENTA = null;
      data.CANTIDAD = 1;
      data.PRECIO = 0;
      data.ID_USUARIO = auth.auth.NOMINA;
    }
    producto.push(data);
    pu.push({ precio: 0, index: data.id });
    cant.push({ cantidad: 1, index: data.id });
    nom.push({ nombre: "", index: data.id });
    setRefreshing(true);
  };

  const deleteAct = (id) => {
    const filtered1 = pu.filter((x) => x.index !== id);
    const filtered2 = cant.filter((x) => x.id !== id);
    const filtered3 = nom.filter((x) => x.id !== id);
    const filtered4 = producto.filter((x) => x.id !== id);
    setProducto(filtered4);
    setPU(filtered1);
    setCant(filtered2);
    setNom(filtered3);
    
    setRefreshing(true);
    calcSubtotal();
    setRefreshing(false);
  };

  const renderItem = ({ item, index }) => {
    setRefreshing(false);
    return (
      <View>
        <View style={styles.container}>
          <Text>Producto</Text>
          <TouchableOpacity
            style={styles.btnDropdown}
            onPress={() => {
              setModal2Visible(true);
              setActualIndex(index + 1);
              console.log("el index es", index);
            }}
          >
            <Text
              style={{
                padding: 7,
                fontSize: 12,
                marginRight: "40%",
                width: 280,
                marginRight: 0,
              }}
            >
              {nom[item.id].nombre != ""
                ? nom[item.id].nombre
                : "Seleccione un producto"}
            </Text>
            <Ionicons
              name="chevron-down"
              size={20}
              style={{ marginTop: 5 }}
            ></Ionicons>
          </TouchableOpacity>

          <Text>Cantidad</Text>
          <View style={{ flexDirection: "row" }}>
            <TextInput
              style={{
                width: 150,
                margin: 15,
                borderWidth: 1,
                borderColor: "#f0f0f0",
                height: 40,
                textAlign: "center",
              }}
              cursorColor="#feb72b"
              keyboardType="numeric"
              onChangeText={(x) => {
                cant[item.id].cantidad = x;
                calcSubtotal();
              }}
            >
              {cant[item.id].cantidad}
            </TextInput>
            <View style={{ marginLeft: 15, alignItems: "center" }}>
              <Text>Precio Unitario</Text>
              
              <Text>${pu[item.id].precio}</Text>
              <TouchableOpacity
            style={{
              backgroundColor: "black",
              height: 30,
              width:100,
              borderRadius:15,
              alignItems: "center",
              justifyContent: "center",
              marginTop:5
            }}
            onPress={() => {
              Alert.alert("", "¿Seguro de eliminar el producto?", [
                {
                  text: "Cancelar",
                  onPress: () => {
                   
                  },
                },
                {
                  text: "Aceptar",
                  onPress: () => {
                    console.log(item.id)
                    deleteAct(item.id);
                  },
                },
              ]);
              }}
          >
            <Ionicons
              style={{  }}
              name="trash-outline"
              color={"white"}
              size={20}
            ></Ionicons>
            
      
              </TouchableOpacity>
            </View>
          </View>
          
        </View>
      </View>
    );
  };

  const loadData = () => {
    if (producto.length == 0) {
      setRefreshing(false);
    } else {
      renderItem();
    }
  };

  const finalizarVenta = async () => {
    let venta = {};
    let error = false;
    if (total == 0) {
      error = true;
    }
    if (!error) {
      venta.ID_CLIENTE = id_cliente;
      (venta.ID_USUARIO = auth.auth.NOMINA),
        (venta.ID_PUNTO_VENTA = auth.auth.ID_PUNTO_VENTA);
      venta.SUBTOTAL = total;
      venta.IMPORTE = total;

      //console.log(venta)
      try {
        let res = await savitarApi.post(`generarVenta`, venta);
        //console.log(res.data)
        producto.forEach((x) => {
          x.ID_VENTA = res.data[0].ID_VENTA;
          x.ID_USUARIO = auth.auth.NOMINA;
          x.ID_PUNTO_VENTA = res.data[0].ID_PUNTO_VENTA;
        });

        let product = {
          PRODUCTOS: JSON.stringify(producto),
        };

        let data = await savitarApi.post(`insertarProductos`, product);

        let data_prod = await axios
          .get(
            `http://savitar.app/WS_savitar.asmx/getURLventaPDF?idVenta=${res.data[0].ID_VENTA}&tipo=1`,
            { responseType: "text" }
          )
          .then(createAlert());
      } catch (error) {
        console.log(error);
      }
    }
  };

  const createAlert = () => {
    Alert.alert("", "Se ha generado la venta", [
      {
        text: "Aceptar",
        onPress: () => {
          setID_Cliente(null);
          setProducto([]);
          setRefreshing(true);
          loadData();
        },
      },
    ]);
  };
  const renderItem2 = ({ item, index }) => {
    setRefreshing(false);
    return (
      <View style={styles.container2}>
        <View style={{ flexDirection: "row" }}>
          <CheckBox
            title=""
            checked={false}
            containerStyle={{
              backgroundColor: "white",
              padding: 5,
              marginLeft: 0,
            }}
            checkedColor={"#F5B102"}
            onPress={() => {
              pu[actualIndex].precio = item.PRECIO_VENTA;
              nom[actualIndex].nombre = item.DESCRIPCION_MODELO;
              setModal2Visible(!modal2Visible);
              calcSubtotal();
              setRefreshing(true);
            }}
          />
          <Text style={{ color: "black", fontSize: 12, marginTop: 5 }}>
            {item.DESCRIPCION_MODELO} {item.ID_MODELO}
          </Text>
        </View>
      </View>
    );
  };
  const loadMore = async () => {
    setInicio(fin + 1);
  };

  const closeModal = () => {
    setModalVisible(!modalVisible);
    setRefreshing(false);
  };
  const closeModal2 = () => setModal2Visible(!modal2Visible);

  return (
    <View style={styles.window}>
      <Modal
        animationType="slide"
        visible={modalVisible}
        transparent={true}
        onRequestClose={() => {
          closeModal();
        }}
      >
        <View
          style={{
            width: "90%",
            alignSelf: "center",
            marginTop: 50,
            height: 630,
          }}
        >
          <Header
            statusBarProps={{ backgroundColor: "#F5B102" }}
            containerStyle={{
              backgroundColor: "#F5B102",
              justifyContent: "space-around",
            }}
            leftComponent={{
              text: "Agregar Cliente",
              style: { color: "#fff", width: 200 },
            }}
            rightComponent={
              <TouchableOpacity onPress={() => closeModal()}>
                <Ionicons
                  name={"close-circle-outline"}
                  color={"#fff"}
                  size={25}
                />
              </TouchableOpacity>
            }
          />
          <Clientes closeModal={() => closeModal()}></Clientes>
        </View>
      </Modal>
      <Modal
        animationType="slide"
        visible={modal2Visible}
        transparent={true}
        onRequestClose={() => {
          closeModal();
        }}
      >
        <View
          style={{
            width: "90%",
            alignSelf: "center",
            marginTop: 50,
            height: 630,
          }}
        >
          <Header
            statusBarProps={{ backgroundColor: "black" }}
            containerStyle={{
              backgroundColor: "black",
              justifyContent: "space-around",
            }}
            leftComponent={{
              text: "Productos",
              style: { color: "#fff", width: 200 },
            }}
            rightComponent={
              <TouchableOpacity onPress={() => closeModal2()}>
                <Ionicons
                  name={"close-circle-outline"}
                  color={"#fff"}
                  size={25}
                />
              </TouchableOpacity>
            }
          />

          <TextInput
            placeholder="Buscar"
            style={{
              backgroundColor: "white",
              height: 50,
              paddingLeft: 15,
              borderBottomColor: "#f0f0f0",
              borderBottomWidth: 2,
            }}
            onChangeText={(x) => {
              /*setInicio(1);
              setFin(cantidadProductos)*/
              setBuscador(x);
            }}
            value={buscador}
          ></TextInput>
          <Button
            buttonStyle={{ backgroundColor: "#F5B102", height: 50 }}
            title="Buscar"
            onPress={() => {
              setInicio(1);
              setFin(cantidadProductos);
              initProductos(buscador);
            }}
          >
            Buscar
          </Button>
          <FlatList
            data={prodInfo}
            keyExtractor={(item) => item.ID_MODELO}
            renderItem={renderItem2}
            onEndReached={loadMore}
            onEndReachedThreshold={0.1}
            ListFooterComponent={ListFooterComponent}
            style={{
              width: "100%",
              marginBottom: 100,
              height: 460,
              backgroundColor: "white",
            }}
          ></FlatList>
        </View>
      </Modal>

      <View
        style={
          modalVisible || modal2Visible
            ? { backgroundColor: "gray", opacity: 0.5, height: 641 }
            : { hide: "true" }
        }
      >
        <Text style={styles.label}>Cliente</Text>

        <View style={{ flexDirection: "row" }}>
          <SelectDropdown
            data={clientes}
            onSelect={async (selectedItem, index) => {
              let testing = clientID.filter((x) => x.Nombre == selectedItem);
              let testid = testing[0].Id.toString();
              //console.log(testid)

              try {
                AsyncStorage.removeItem("id_cliente");
                AsyncStorage.setItem("id_cliente", testid);
                let localID = await AsyncStorage.getItem("id_cliente");
                setID_Cliente(localID);
              } catch (error) {
                console.log(error);
              }
            }}
            defaultButtonText={"Seleccione el cliente"}
            buttonTextAfterSelection={(selectedItem, index) => {
              return selectedItem;
            }}
            rowTextForSelection={(item, index) => {
              return item;
            }}
            buttonStyle={styles.btnCliente}
            buttonTextStyle={styles.dropdown1BtnTxtStyle}
            renderDropdownIcon={(isOpened) => {
              return (
                <Ionicons
                  name={isOpened ? "chevron-up" : "chevron-down"}
                  color={"#444"}
                  size={18}
                />
              );
            }}
            dropdownIconPosition={"right"}
            dropdownStyle={styles.dropdown1DropdownStyle}
            rowStyle={styles.dropdown1RowStyle}
            rowTextStyle={styles.dropdown1RowTxtStyle}
            selectedRowStyle={styles.dropdown1SelectedRowStyle}
            search
            searchInputStyle={styles.dropdown1searchInputStyleStyle}
            searchPlaceHolder={"Search here"}
            searchPlaceHolderColor={"darkgrey"}
            renderSearchInputLeftIcon={() => {
              return <Ionicons name={"search"} color={"#444"} size={18} />;
            }}
          />
          <TouchableOpacity
            style={styles.btnNew}
            onPress={() => {
              setModalVisible(true);
              setRefreshing(true);
            }}
          >
            <Ionicons name={"add-circle-outline"} size={30} color={"#fff"} />
          </TouchableOpacity>
        </View>

        <TouchableOpacity
          style={styles.btnProd}
          onPress={() => agregarProducto()}
        >
          <Text style={{ color: "white" }}>Agregar Producto</Text>
        </TouchableOpacity>

        <FlatList
          data={producto}
          keyExtractor={(item) => item.id}
          renderItem={renderItem}
          refreshing={refreshing}
          onRefresh={loadData}
          style={{
            width: "100%",
            marginBottom: 50,
            marginTop: 10,
            height: 430,
          }}
        ></FlatList>
        <TouchableOpacity
          style={subtotal == 0 ? styles.btnDisable : styles.btnFinal}
          refreshing={refreshing}
          onPress={() => {
            finalizarVenta();
          }}
          disabled={subtotal == 0 ? true : false}
        >
          <Text style={{ color: "white" }}>Finalizar Pedido ${subtotal}</Text>
        </TouchableOpacity>
      </View>

      <StatusBar style="light" />
    </View>
  );
}

const styles = StyleSheet.create({
  window: {
    height: Dimensions.get("window").height - 120,
    backgroundColor: "white",
  },
  container: {
    backgroundColor: "white",
    margin: 10,
  },
  container2: {
    flex: 1,
    backgroundColor: "white",
    padding: 7,
  },
  label: {
    fontSize: 16,
    marginLeft: 15,
    marginTop: 15,
  },
  btnCliente: {
    borderWidth: 1,
    borderColor: "#E3E3E3",
    borderRadius: 10,
    marginLeft: 15,
    marginRight: 15,
    width: "60%",
    height: 35,
  },
  btnDropdown: {
    borderWidth: 1,
    borderColor: "#E3E3E3",
    borderRadius: 10,
    margin: 10,
    width: "94%",
    height: 45,
    backgroundColor: "white",
    flexDirection: "row",
  },

  btnNew: {
    backgroundColor: "#F5B102",
    padding: 2,
    marginLeft: 15,
    width: 70,
    alignItems: "center",
    borderRadius: 10,
  },
  btnProd: {
    backgroundColor: "#F5B102",
    padding: 7,
    width: "80%",
    alignItems: "center",
    borderRadius: 20,
    alignSelf: "center",
    marginTop: 40,
    overflow: "visible",
    elevation: 8, // Android
  },
  btnDisable: {
    position: "absolute",
    bottom: 0,
    backgroundColor: "#F5B102",
    opacity: 0.5,
    padding: 7,
    width: "98%",
    alignItems: "center",
    borderRadius: 20,
    alignSelf: "center",
    overflow: "visible",
    elevation: 8, // Android
  },
  btnFinal: {
    position: "absolute",
    bottom: 0,
    backgroundColor: "#F5B102",
    padding: 7,
    width: "98%",
    alignItems: "center",
    borderRadius: 20,
    alignSelf: "center",
    overflow: "visible",
    elevation: 8, // Android
  },
  dropdown1BtnTxtStyle: { color: "#444", textAlign: "left", fontSize: 12 },
  dropdown1DropdownStyle: { backgroundColor: "#EFEFEF" },
  dropdown1RowStyle: {
    backgroundColor: "#EFEFEF",
    borderBottomColor: "#C5C5C5",
  },
  dropdown1RowTxtStyle: { color: "#444", textAlign: "left", fontSize: 12 },
  dropdown1SelectedRowStyle: { backgroundColor: "rgba(0,0,0,0.1)" },
  dropdown1searchInputStyleStyle: {
    backgroundColor: "#EFEFEF",
    borderRadius: 8,
    borderBottomWidth: 1,
    borderBottomColor: "#444",
  },
  rowLeft: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "flex-end",
    alignItems: "center",
    paddingLeft: 20,
    paddingRight: 20,
    backgroundColor: "#FE4D33",
  },
});

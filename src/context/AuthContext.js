import axios from "axios";
import React, { createContext, useReducer } from "react";
import { AsyncStorage } from "react-native";
import {savitarApi} from "../api/savitarApi";
import { authReducer, initialAuthState } from "./AuthReducer";

export const AuthContext = createContext({});

export const AuthProvider = ({children}) => {

    const [state, dispatch] = useReducer(authReducer, initialAuthState)

    const signIn = async (user, pwd) => {
        const data = {"USUARIO": user, "PASSWORD": pwd};
        try{
           
            const res = await savitarApi.get(`/login`,{data});
         

            if(!res.data){
                console.log('Alerta: ' )
            }else{
                dispatch({type: 'fetch_user_success', payload: {user: res.data[0]}});
                //console.log('ok');
            }
            
        }catch(error){
            console.log('err' + error);
        }
    };
    const logOut = async () => {
        //await AsyncStorage.removeItem('user');
        dispatch({type: 'user_sign_out'});
    }; 

    return (
        <AuthContext.Provider value={{
            ...state,
            signIn,
            logOut
        }}>
            {children}
        </AuthContext.Provider>
    );
}
import { createContext } from "react";
import { logOut, signIn } from "../store/actions/AuthActions";


const ApiContext = createContext(null);

const ApiProvider = ({children}) => {
    let val = {
        api:{
            signIn: (user,pwd) => (dispatch) => signIn(user,pwd)(dispatch),
            logOut: () => (dispatch) => logOut()(dispatch),
/*
            saveUserLocation: (user,location) => (dispatch) => saveUserLocation(user,location)(dispatch),
            getUsers: () => (dispatch) => getUsers()(dispatch),

            getChartReport: (userId) => (dispatch) => getChartReport(userId)(dispatch),
            getDataReport: (userId) => (dispatch) => getDataReport(userId)(dispatch)*/
        }
    }

    return (
        <ApiContext.Provider value={val}>
            {children}
        </ApiContext.Provider>
    );
}

export {
    ApiContext,
    ApiProvider
}
import axios from "axios";

/* PRUEBAS */
//const baseURL = 'http://localhost:18798';



/* PRODUCCIÓN */
const urlTicket= 'http://savitar.app/WS_savitar.asmx';
const url_descarga= "https://savitar.app/Docs/pedidos/";
const baseURL = 'http://apisavitar.maindsoft.club';

export const savitarApi = axios.create({baseURL});
export const savitarTicket = axios.create({urlTicket , timeout : 1000});
export const descargarTicket = axios.create({url_descarga});


//export default (savitarApi, savitarTicket, descargarTicket);
import { Provider, useDispatch } from "react-redux";
import { AuthProvider } from "./src/context/AuthContext";
import { NavigationContainer } from "@react-navigation/native";
import Navigator from "./src/navigation/navigator";
import AppCommon from "./AppCommon";
import Store from "./src/store/Store";
import { ApiProvider } from "./src/context/ApiContext";
import React, { useState, useEffect } from "react";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { ActivityIndicator, Dimensions, StyleSheet, View } from "react-native";
import { MenuProvider } from "react-native-popup-menu";
import { RootSiblingParent } from "react-native-root-siblings";
import { SafeAreaView } from "react-native-safe-area-context";

const AppState = ({ children }) => {
  return <AuthProvider>{children}</AuthProvider>;
};

export default function App() {
  const [assetsLoaded, setassetsLoaded] = useState(false);
  // const dispatch = useDispatch();

  useEffect(() => {
    onLoad();
  }, []);

  const _loadResourcesAsync = async () => {
    return Promise.all([
      AsyncStorage.getItem("user", (err, res) => {
        // console.log({'store': res});
        //return res;
      }),
    ]);
  };

  const onLoad = async () => {
    if (__DEV__) {
      AsyncStorage.getItem("user", (err, res) => {
        // if(res != null){
        //   dispatch({type: 'fetch_user_success', payload: res});
        // }
        // console.log({'store': res});
        //return res;
        setassetsLoaded(true);
      });
    } else {
      try {
        AsyncStorage.getItem("user", (err, res) => {
          // console.log({'store': res});
          // if(res != null){
          //   dispatch({type: 'fetch_user_success', payload: res});
          // }
          //return res;
          setassetsLoaded(true);
        });
        // _loadResourcesAsync().then(() => setassetsLoaded(true));
      } catch (err) {
        AsyncStorage.getItem("user", (err, res) => {
          // console.log({'store': res});
          // if(res != null){
          //   dispatch({type: 'fetch_user_success', payload: res});
          // }
          //return res;
          setassetsLoaded(true);
        });
        // _loadResourcesAsync().then(() => setassetsLoaded(true));
      }
    }
  };
  if (!assetsLoaded) {
    return (
      <View style={styles.container}>
        <ActivityIndicator style={{ paddingBottom: 100 }} size="large" />
      </View>
    );
  }
  return (
    <RootSiblingParent>
      <Provider store={Store}>
        <ApiProvider>
        <AppCommon>
            
              <NavigationContainer>
                <Navigator></Navigator>
              </NavigationContainer>
            
            </AppCommon>
        </ApiProvider>
      </Provider>
    </RootSiblingParent>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#fff",
  },
});

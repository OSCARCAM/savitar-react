import { useState, useContext, useEffect, useRef } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { ApiContext } from './src/context/ApiContext';
import AsyncStorage from '@react-native-async-storage/async-storage';
import AuthLoadingScreen from './src/screens/AuthLoadingScreen';





export default function AppCommon({children}){

    const { api } = useContext(ApiContext);
    const {  signIn } = api;
  
    const auth = useSelector(state => state.auth);

    const authStillNotResponded = useRef(false);
    const [load, setLoad] = useState(false);


    const authState = useRef('loading');
   
    const dispatch = useDispatch();

    useEffect(() => {
        
       getUser();
    }, []);

    const getUser = async () => {
        try{
            const res = await AsyncStorage.getItem('user');
        
            if(res){
                let auxRes = JSON.parse(res);
                
                dispatch(signIn(auxRes.CORREO, auxRes.PASSWORD));
                setTimeout(() => {
                    setLoad(true)
                }, 350);
            }else{
                setTimeout(() => {
                    setLoad(true)
                }, 350);
            }
        }catch(e){
            setLoad(true);
            console.log(e);
        }
        
    }   
    if(!load){
        return <AuthLoadingScreen />
    }

    return children;
}